var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
var issue = {title: "New Issue", content: "Issue content"};
var version = "First version";

describe('Repository', function() {
  var repo;

  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  describe(".versions()", function () {
    var versionId = null;

    describe(".create()", function () {
      it('should create a new version', function (done) {
        repo.versions().create(version, function (err, result) {
          versionId = result.id;
          result.name.should.eql(version);
          done(err);
        });
      });
    });

    describe(".getById()", function () {
      it('should get the version by the id', function (done) {
        repo.versions().getById(versionId, function (err, result) {
          result.name.should.eql(version);
          result.id.should.eql(versionId);
          done(err);
        });
      });
    });

    describe(".getAll()", function () {
      it('should get all versions', function (done) {
        repo.versions().getAll(function (err, result) {
          Array.isArray(result).should.be.ok;
          done(err);
        });
      });
    });

    describe(".update()", function () {
      it('should update a version', function (done) {
        repo.versions().update(versionId, "Updated content", function (err, result) {
          result.name.should.eql("Updated content");
          done(err);
        });
      });
    });

    describe(".remove()", function () {
      it('should remove a given version', function (done) {
        repo.versions().remove(versionId, function (err, result) {
          (result === null).should.be.ok;
          done(err);
        });
      });
    });

    after(function(done) {
      repo.versions().getAll(function (err, versions) {
        if (versions.length === 0) { return done(); }
        versions.forEach(function (m) {
          repo.versions().remove(m.id, function (err, result) {
            done(err);
          });
        })
      });
    });

  });
});
