var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;

describe('Repository', function() {
  var repo;
  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  describe(".followers()", function () {
    it('should return the followers for the repository', function (done) {
      repo.followers(function (err, result) {
        result.count.should.not.eql(0);
        Array.isArray(result.followers).should.be.ok;
        done(err);
      });
    });
  });
});
